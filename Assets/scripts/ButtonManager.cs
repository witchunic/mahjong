﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;

public class ButtonManager : MonoBehaviour {

    public static ButtonManager instance;

    void Awake()
    {
        instance = this;
        //init();
    }

    public GameObject[] buttons = new GameObject[15];
    public GameObject[] discardLayouts = new GameObject[5];
    public GameObject PassButton;
    public List<GameObject[]> combs = new List<GameObject[]>();

    public GameObject CombButtonLayout;

    public void Init()
    {
        for (int i = 0; i < 15; i++)
        {
            buttons[i] = GameObject.Find((i + 1).ToString());
        }

        for (int i = 0; i < PlayerBase.instance.player_count; i++)
        {

            discardLayouts[i] = GameObject.Find("discardLayout" + (i + 1));

            GameObject[] temp = new GameObject[5];

            for (int a = 0; a < 5; a++)
            {

                temp[a] = GameObject.Find("Comb" + (i + 1) + "-" + (a + 1));

                temp[a].SetActive(false);
            }

            combs.Add(temp);
        }



        CombButtonLayout = GameObject.Find("CombLayout");
    }

    public void ButtonMaker()
    {

        for (int i = 0; i < 15; i++)
        {
            if (PlayerBase.instance.GetTile(0, i) == tile_system.instance.Zero)
                buttons[i].SetActive(false);
            else
            {
                buttons[i].SetActive(true);
                spriteManager.instance.SpriteSetter(buttons[i], PlayerBase.instance.GetTile(0, i), false);
            }
        }
    }

    public void CombDeclared(int player, int index, tile_system.comb comb)
    {

        combs[player][index].SetActive(true);

        spriteManager.instance.SpriteSetter(combs[player][index], comb.Tiles[0], true);


        if ((int)comb.Type == 1)
        {
            combs[player][index].transform.Find("chi").gameObject.SetActive(true);
        }
        else if ((int)comb.Type == 3)
        {
            combs[player][index].transform.Find("pon").gameObject.SetActive(true);
        }
        else if ((int)comb.Type == 4)
        {
            combs[player][index].transform.Find("kan").gameObject.SetActive(true);
        }
        else if ((int)comb.Type == 6)
        {
            combs[player][index].transform.Find("MJ").gameObject.SetActive(true);
            GameObject.Find("butGroup").SetActive(false);
        }


    }

    public void CheckRiichiButton()
    {
       /* if (PlayerBase.instance.GetTenpai(0))
            game_system.instance.Riichi.SetActive(true);
        else
            game_system.instance.Riichi.SetActive(false);*/

    }

    public void CallRiichi()
    {
        PlayerBase.instance.SetRiichi(0, true);
    }

    public void ClearCombButtons()
    {
        foreach (Transform child in CombButtonLayout.transform)
        {

            Destroy(child.gameObject);
        }
    }

    public void WaitingManager(int player, List<tile_system.Tile> tiles)
    {
        if (player == 0)
        {
            WaitingsClear();

            foreach (var item in tiles)
            {
                GameObject tempButton = Instantiate(game_system.instance.Tile);
                tempButton.GetComponent<RectTransform>().sizeDelta = new Vector2(25, 50);
                spriteManager.instance.SpriteSetter(tempButton, item, false);
                tempButton.transform.SetParent(game_system.instance.Waitings.transform, false);
            }


        }
    }

    public void WaitingsClear()
    {
        foreach (Transform child in game_system.instance.Waitings.transform)
        {

            Destroy(child.gameObject);
        }
    }

    public void CombButtonCreator(int player, bool FromDiscard, List<tile_system.comb> CombList)
    {
        int possibleDeclarations = 0;
        ExecutionManager.instance.StoreCurrentCombs(CombList);
        for (int i = 0; i < CombList.Count; i++)
        {
            if ((int)ExecutionManager.instance.GetStorenCombs()[i].Type == 1 && FromDiscard)
            {
                if (player != 0)
                {
                    if (player != PlayerBase.instance.CurrentPlayer + 1)
                        continue;
                }
                else if (player == 0 && PlayerBase.instance.CurrentPlayer != PlayerBase.instance.player_count-1)
                    continue;
            }

            if ((int)ExecutionManager.instance.GetStorenCombs()[i].Type == 2 || (int)ExecutionManager.instance.GetStorenCombs()[i].Type == 5)
                continue;

            if ((int)ExecutionManager.instance.GetStorenCombs()[i].Type == 4 && tile_system.instance.KanCounter >= 4)
                continue;

            if (!FromDiscard && ((int)ExecutionManager.instance.GetStorenCombs()[i].Type < 4))
                continue;

            if (PlayerBase.instance.IsRiichi(player) && (int)ExecutionManager.instance.GetStorenCombs()[i].Type != 6)
                continue;

            bool duplicate = false;

            if (i > 0)
            {
                for (int checkDuplicate = 0; checkDuplicate < i; checkDuplicate++)
                {
                    if (CombList[i].Type == CombList[checkDuplicate].Type)
                    {
                        if (CombList[i].Tiles[0].Number == CombList[checkDuplicate].Tiles[0].Number)
                        {
                            duplicate = true;
                            break;
                        }
                    }
                }
            }

            if (duplicate)
                continue;

            possibleDeclarations++;

            GameObject tempButton = GameObject.Instantiate(game_system.instance.CombButton);
            tempButton.transform.SetParent(CombButtonLayout.transform, false);

            spriteManager.instance.SpriteSetter(tempButton, ExecutionManager.instance.GetStorenCombs()[i].Tiles[0], true);


            if ((int)ExecutionManager.instance.GetStorenCombs()[i].Type == 1)
            {
                tempButton.transform.Find("chi").gameObject.SetActive(true);
            }
            else if ((int)ExecutionManager.instance.GetStorenCombs()[i].Type == 3)
            {
                tempButton.transform.Find("pon").gameObject.SetActive(true);

            }
            else if ((int)ExecutionManager.instance.GetStorenCombs()[i].Type == 4)
            {
                tempButton.transform.Find("kan").gameObject.SetActive(true);
            }
            else if ((int)ExecutionManager.instance.GetStorenCombs()[i].Type == 6)
            {
                tempButton.transform.Find("MJ").gameObject.SetActive(true);
            }


            int currentCycle = i;
            if (FromDiscard)
                tempButton.GetComponent<Button>().onClick.AddListener(() => ExecutionManager.instance.PlayerAct(ExecutionManager.instance.GetStorenCombs()[currentCycle]));
            else
                tempButton.GetComponent<Button>().onClick.AddListener(() => ExecutionManager.instance.CombExecution(0, ExecutionManager.instance.GetStorenCombs()[currentCycle]));


            EventTrigger EventTriggerComponent = tempButton.GetComponent<EventTrigger>();

            //--- ON POINTER ENTER
            EventTrigger.TriggerEvent trigger = new EventTrigger.TriggerEvent();

            trigger.AddListener((idChild) => { game_system.instance.HoverOverCombButton(true, ExecutionManager.instance.GetStorenCombs()[currentCycle]); });

            EventTrigger.Entry entry = new EventTrigger.Entry() { callback = trigger, eventID = EventTriggerType.PointerEnter };
            EventTriggerComponent.triggers.Add(entry);

            //---- ON POINTER EXIT
            EventTrigger.TriggerEvent trigger2 = new EventTrigger.TriggerEvent();

            trigger2.AddListener((eventData) => { game_system.instance.HoverOverCombButton(false, ExecutionManager.instance.GetStorenCombs()[currentCycle]); });

            EventTrigger.Entry entry2 = new EventTrigger.Entry() { callback = trigger2, eventID = EventTriggerType.PointerExit };
            EventTriggerComponent.triggers.Add(entry2);


        }

        if (possibleDeclarations == 0)
        {
            ExecutionManager.instance.PlayerAct(null);
        }
    }

    public void ColorSelection(int player, tile_system.Tile tile, bool turnOn)
    {
        int index = PlayerBase.instance.GetHand(0, 0, true).ToList().IndexOf(tile);


        if (turnOn)
            buttons[index].GetComponentsInChildren<Image>()[1].enabled = true;
        else
            buttons[index].GetComponentsInChildren<Image>()[1].enabled = false;

    }

    public void ColorClean()
    {
        foreach (var item in buttons)
        {
            item.GetComponentsInChildren<Image>()[1].enabled = false;
        }
    }

    public void DiscardManager(int player, tile_system.Tile tile, bool isStolen)
    {
        GameObject tempButton = GameObject.Instantiate(game_system.instance.Tile);
        tempButton.GetComponent<RectTransform>().sizeDelta = new Vector2(25, 50);
        spriteManager.instance.SpriteSetter(tempButton, tile, false);
        tempButton.transform.SetParent(discardLayouts[player].transform, false);

        if (isStolen)
            tempButton.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);

    }

    public void CountOperator(int i)
    {
        game_system.instance.counter.text = i.ToString();
    }

    public void TilesLeftOperator()
    {
        game_system.instance.tilesLeftCounter.text = tile_system.instance.TilesLeft.ToString();
    }
}
