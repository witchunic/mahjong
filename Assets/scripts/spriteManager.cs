﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;


public class spriteManager : MonoBehaviour {


    public List<Sprite> sprites = new List<Sprite>();
    public List<Sprite> suits = new List<Sprite>();

	public static spriteManager instance;

        void Awake()
    {
        instance = this;
    }

    public void SpriteSetter(GameObject GUITile, tile_system.Tile tile, bool isComb)
    {
        
        switch ((int)tile.Suit)
        {
            default:
                singleTileColor(GUITile, "", Color.black, 3 /*Color.white*/, isComb);
                break;
            case 1:
                singleTileColor(GUITile, tile.Number.ToString(), Color.white, 0 /*Color.red*/, isComb);
                break;
            case 2:
                singleTileColor(GUITile, tile.Number.ToString(), Color.white, 1 /*Color.green*/, isComb);
                break;
            case 3:
                singleTileColor(GUITile, tile.Number.ToString(), Color.white, 2 /*Color.blue*/, isComb);
                break;
            case 4:
                singleTileColor(GUITile, "D", Color.red, 3 /*Color.white*/, isComb);
                break;
            case 5:
                singleTileColor(GUITile, "D", Color.green, 3 /*Color.white*/, isComb);
                break;
            case 6:
                singleTileColor(GUITile, "D", Color.blue, 3 /*Color.white*/, isComb);
                break;
            case 7:
                singleTileColor(GUITile, "E", Color.black, 3 /*Color.white*/, isComb);
                break;
            case 8:
                singleTileColor(GUITile, "N", Color.black, 3 /*Color.white*/, isComb);
                break;
            case 9:
                singleTileColor(GUITile, "W", Color.black, 3 /*Color.white*/, isComb);
                break;
            case 10:
                singleTileColor(GUITile, "S", Color.black, 3 /*Color.white*/, isComb);
                break;
            
        }
        
    }

    void singleTileColor(GameObject GUITile, string name, Color textColor, int TileColor /* Color tileColor*/, bool isComb)
    {
        int Color = (isComb) ? TileColor + 4 : TileColor;

        // GUITile.GetComponent<Image>().color = tileColor;
        GUITile.GetComponent<Image>().sprite = suits[Color];

        GUITile.GetComponentInChildren<Text>().text = name;
        GUITile.GetComponentInChildren<Text>().color = textColor;
    }

}
