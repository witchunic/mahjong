﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Linq;

public class TileGUI : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (PlayerBase.instance.CurrentPlayer == 0 && game_system.instance.State == game_system.GameState.playerTurn)
        {
            if ((game_system.instance.kostyl.Contains(gameObject.name) && !PlayerBase.instance.IsRiichi(0)) || (PlayerBase.instance.IsRiichi(0) && gameObject.name == "14"))
            {
                game_system.instance.Play(0);
                GetComponent<LayoutElement>().ignoreLayout = true;
                GetComponent<CanvasGroup>().blocksRaycasts = false;
            }
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (PlayerBase.instance.CurrentPlayer == 0 && game_system.instance.State == game_system.GameState.playerTurn)
        {
            if ((game_system.instance.kostyl.Contains(gameObject.name) && !PlayerBase.instance.IsRiichi(0)) || (PlayerBase.instance.IsRiichi(0) && gameObject.name == "14"))
                this.transform.position = eventData.position; }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (PlayerBase.instance.CurrentPlayer == 0 && game_system.instance.State == game_system.GameState.playerTurn)
        {
            if ((game_system.instance.kostyl.Contains(gameObject.name) && !PlayerBase.instance.IsRiichi(0)) || (PlayerBase.instance.IsRiichi(0) && gameObject.name == "14"))
            {
                game_system.instance.Play(1);
                GetComponent<LayoutElement>().ignoreLayout = false;
                GetComponent<CanvasGroup>().blocksRaycasts = true;
            }
        }
    }
    
}
