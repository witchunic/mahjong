﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

public class tile_system : MonoBehaviour {

    public static tile_system instance;

    public TilePrototype[] tilesBase = new TilePrototype[35];
    public Tile[] deck = new Tile[136];
    public int currentTile = 0;
    Tile lastDrop;
    int deadWallCounter = 121;
    int kanCounter = 0;
    Tile zero;

    public class TilePrototype
    {
        int id;
        int number;
        suit suit;
        tileType type = tileType.none;
        bool honor = false;

        public TilePrototype(int id, int number, suit suit)
        {
            this.id = id;
            this.number = number;
            this.suit = suit;

            if ((int)suit > 3)
            {
                honor = true;
                if ((int)suit > 6)
                {
                    type = tileType.dragon;
                }
                else
                {
                    type = tileType.wind;
                }
            }
            else
            {
                if (number == 9 || number == 1)
                    type = tileType.terminal;
            }
        }

        public int Id
        {
            get
            {
                return id;
            }
        }

        public int Number { get { return number; } }

        public suit Suit
        {
            get
            {
                return suit;
            }
        }

        public string Info
        {
            get
            {
                {
                    return number + " of " + suit;
                }
            }
        }

        public tileType Type { get { return type; } }
        public bool Honor { get { return honor; } }

        
    }

    public class Tile
    {
        int uid;
        TilePrototype prot;

        public Tile (int uid, TilePrototype prot)
        {
            this.uid = uid;
            this.prot = prot;
        }

        public int ID { get { return prot.Id; } }
        public int Uid { get { return uid; } }
        public int Number { get { return prot.Number; } }
        public suit Suit { get { return prot.Suit; } }
        public string Info { get { return prot.Info; } } 
        public tileType Type { get { return prot.Type; } }
        public bool Honor { get { return prot.Honor; } }

    }

    public enum tileType { none, terminal, wind, dragon };

    public enum CombType
    {
        none = 0,
        pair = 2,
        pon = 3,
        kan = 4,
        chi = 1,
        halfChi = 5,
        mahjong = 6
    }

    public class comb
    {
        CombType combType;
        Tile[] tiles;
        bool honor = false;
        tileType classification = tileType.none;

        public comb(CombType combType, Tile[] tiles )
        {
            this.combType = combType;
            this.tiles = tiles;

            classification = tiles[0].Type;
            honor = tiles[0].Honor;
        }

        public CombType Type { get { return combType; } }

        public Tile[] Tiles { get { return tiles; } }


        public string Info { get { return combType + " of " + tiles[0].Info; } }
    }

    public List<comb> CombFilter (List<comb> combs, int type)
    {
        return combs.Where(cmb => (int)cmb.Type == type).ToList();
    }

    public enum suit { none, characters, bamboo, dots, red, green, white, east, north, west, south }

    void Awake() { instance = this; }

    /*void Start()
    {
        if (game_system.instance.rules == game_system.RuleSet.riichi)
            tilesBase = new TilePrototype[35];
        else
            tilesBase = new TilePrototype[36];
    }*/
    
	public void TilesetInit()
        {
            PrepareBase();
            PrepareSet();
            DeckShuffle(10);
            lastDrop = Zero;
        }

    void PrepareBase()
        {
            int tileCounter = 0;

            TileCreator(0, 0, tileCounter);
            tileCounter++;

            
                for (int i = 1; i <= 10; i++)
                {
                    if (i <= 3)
                    {
                        for (int number = 1; number <= 9; number++)
                        {
                            TileCreator(number, i, tileCounter);
                            tileCounter++;
                        }
                    }
                    else
                    {
                        TileCreator((i + 6), i, tileCounter);
                        tileCounter++;
                    }
                }
        }

      void PrepareSet()
        {
            int count = 1;
            for (int repeat = 0; repeat < 4; repeat++)
            {
                for (int i = 1; i < tilesBase.Length; i++)
                {
                    deck[count-1] = new Tile(count, tilesBase[i]);
                    count++;
                }
            }

            zero = new Tile(0, tilesBase[0]);

        }

        void TileCreator(int number, int suit, int id)
        {
            tilesBase[id] = new TilePrototype(id, number, (suit)suit);
        }

        void DeckShuffle(int times)
        {
            for (int i = 0; i < times; i++)
            {
                int counter = deck.Count() - 1;

                while (counter >= 0)
                {
                    int randomTile = Random.Range(0, counter - 1);

                    Tile TempSlot = deck[randomTile];
                    deck[randomTile] = deck[counter];
                    deck[counter] = TempSlot;
                    counter--;
                }
            }
        }

        public Tile currentDeal()
        {
            if (currentTile < deadWallCounter)
                return deck[currentTile++];
            else
                game_system.instance.DrawState();

            return null;
        }

        public void deal(int player, bool fromDead)
        {
            if (fromDead)
            {
                PlayerBase.instance.DealTile(player, 13, deck[deadWallCounter + kanCounter]);
                ButtonManager.instance.ButtonMaker();
                deadWallCounter--;
            }
            else
            {
                PlayerBase.instance.DealTile(player, 13, currentDeal());

                if (!PlayerBase.instance.CheckAI(player))
                {
                    ButtonManager.instance.ButtonMaker();
                }
            }

            ButtonManager.instance.TilesLeftOperator();
        }

        public void deal()
        {
            for (int player = 0; player < PlayerBase.instance.player_count; player++)
            {
                for (int i = 0; i < 13; i++)
                {
                    PlayerBase.instance.DealTile(player, i, currentDeal());
                }
            }
        }

        public Tile Zero { get { return zero; } }


        public Tile LastDrop
        {
            get { return lastDrop; }
            set
            {
                lastDrop = value;

                PlayerBase.instance.SetLastDiscard(value);
            }
        }


     public Tile GetTileById(int id)
        {
            return new Tile (1, tilesBase[id]);
        }

        public int KanCounter { get { return kanCounter; } }

        public int TilesLeft { get { return deadWallCounter - currentTile; } }
    

    public void DiscardByButton (int ButtonID)
    {
        if (game_system.instance.State == game_system.GameState.playerTurn)
            PlayerBase.instance.Discard(PlayerBase.instance.CurrentPlayer, ButtonID);
    }

   
}
