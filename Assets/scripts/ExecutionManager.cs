﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ExecutionManager : MonoBehaviour {

    public static ExecutionManager instance;

    void Awake() {
        instance = this;

    }

    void Start()
    {
        PrepareToAct();
        PrepareToRequest();
    }


    public bool playerActed;
    public tile_system.comb playerChosen;

    public int[] requestType;
    public tile_system.comb[] requestCombs;

    public List<tile_system.comb> CurrentCombs = new List<tile_system.comb>();

    public void PrepareToAct()
    {
        playerActed = false;
        playerChosen = null;
    }

    public void PrepareToRequest()
    {
        requestType = new int[] { 0, 0, 0, 0 };
        requestCombs = new tile_system.comb[] { null, null, null, null };
    }

    public void PlayerAct(tile_system.comb comb)
    {
        if (comb == null)
            print("Nothing to play!");
        else
            game_system.instance.Play(2);

        playerActed = true;
        playerChosen = comb;
        ButtonManager.instance.ClearCombButtons();
        ButtonManager.instance.ColorClean();
    }

    public bool HasPlayerActed()
    {
        return playerActed;
    }

    public tile_system.comb GetChosen()
    {
        return playerChosen;
    }

    public void SetRequest(int player, tile_system.comb comb)
    {
        if (comb != null)
        {
            requestType[player] = (int)comb.Type;
            requestCombs[player] = comb;
        }
        else
        {
            requestType[player] = 0;
            requestCombs[player] = null;
        }
    }

    public int isPlayerRequested(int player)
    {
        return requestType[player];
    }

    public tile_system.comb GetRequested(int player)
    {
        return requestCombs[player];
    }

    public int GetPriorityPlayer()
    {
        string debug = "";

        for (int i = 0; i < 4; i++)
        {
            debug += "Player" + (i + 1) + " has requested :";
            if (requestType[i] > 0)
            {
                debug += requestCombs[i].Info;
            }
            else
                debug += " nothing.";
        }

        print(debug);

        int playerPriority = 0;
        int currentComb = 0;
        for (int i = 0; i < 4; i++)
        {
            if (currentComb == 0)
            {
                if (requestType[i] > 0)
                {
                    playerPriority = i;
                    currentComb = requestType[i];
                }
                else
                    continue;
            }
            else
            {
                if (requestType[i] == 6)
                {
                    playerPriority = i;
                    currentComb = requestType[i];
                }
                else if (requestType[i] > 2 && currentComb < 3)
                {
                    playerPriority = i;
                    currentComb = requestType[i];
                }
                else if (requestType[i] == 1 && currentComb == 0)
                {
                    playerPriority = i;
                    currentComb = requestType[i];
                }
            }
        }

        return playerPriority;
    }

    public void StoreCurrentCombs(List<tile_system.comb> combs)
    {
        CurrentCombs = combs;
    }

    public List<tile_system.comb> GetStorenCombs()
    {
        return CurrentCombs;
    }

    public void PriorityExecution()
    {
        int priority = GetPriorityPlayer();
        if (requestCombs[priority] != null)
        {
            print("Player  " + (priority + 1) + " Executed " + requestCombs[priority].Info);
            ButtonManager.instance.DiscardManager(PlayerBase.instance.CurrentPlayer, tile_system.instance.LastDrop, true);
            CombExecution(priority, GetRequested(priority));

        }
        else
        {
            ButtonManager.instance.DiscardManager(PlayerBase.instance.CurrentPlayer, tile_system.instance.LastDrop, false);
            game_system.instance.GameControl(false, 0);
        }
    }

    public void DiscardHappened(tile_system.Tile tile)
    {
        tile_system.instance.LastDrop = tile;
        ButtonManager.instance.ButtonMaker();
        game_system.instance.StateSwitcher(true);

        game_system.instance.StartCoroutine(game_system.instance.Discard(tile, PlayerBase.instance.CurrentPlayer));
    }

    public void CombExecution(int player, tile_system.comb comb)
    {
        game_system.instance.Play(3);

        if (!PlayerBase.instance.CheckAI(player))
        {
            ButtonManager.instance.ColorClean();
        }
        if (comb != null)
            PlayerBase.instance.DeclareCombs(player, comb.Type, comb.Tiles);

        if (!PlayerBase.instance.CheckAI(player))
        {
            ButtonManager.instance.ButtonMaker();
            ButtonManager.instance.ClearCombButtons();
        }

        PrepareToRequest();
        ButtonManager.instance.CombDeclared(player, PlayerBase.instance.GetDeclaredCombs(player).Count - 1, comb);
        ButtonManager.instance.ButtonMaker();

        game_system.instance.StateSwitcher(false);
        if ((int)comb.Type == 4)
            tile_system.instance.deal(player, true);

        PlayerBase.instance.TurnSwitch(true, player);

        if (player > 0)
            Ai_system.instance.Ai_Actons(PlayerBase.instance.CurrentPlayer);
    }

}

