﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class game_system : MonoBehaviour {

    public static game_system instance;

    public GameObject CombButton;
    public GameObject Tile;
    public GameObject Riichi;
    public GameObject Waitings;
    public List<GameObject> CombPrefabs;
    public RuleSet rules = RuleSet.mcr;
    
    public Text counter;
    public Text tilesLeftCounter;
    public string[] kostyl = new string[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14" };

    public List<AudioClip> sounds;
    public AudioSource player;


    

    GameState currentGameState = GameState.None;

    public void Play(int id)
    {
        player.clip = sounds[id];
        player.Play();
    }

    public enum GameState {None, playerTurn, DiscardCheck };
    public enum RuleSet { riichi, mcr };

    void Awake ()
    {
        instance = this;
    }

    void Start()
    {
        // tileSet = new TileSet();
        tile_system.instance.TilesetInit();
        PlayerBase.instance.CreatePlayers(PlayerBase.instance.player_count, PlayerBase.instance.ai_players);
        PlayerBase.instance.StarterDeal();
        tile_system.instance.LastDrop = tile_system.instance.Zero;
        ButtonManager.instance.Init();
        GameControl(false, 0);
    }

        

        public void GameControl(bool stealTurn, int player)
        {
            if (stealTurn)
                PlayerBase.instance.TurnSwitch(true, player);
            else
                PlayerBase.instance.TurnSwitch(false, 0);

            print("Player " + (PlayerBase.instance.CurrentPlayer + 1) + " turn");
        ButtonManager.instance.WaitingsClear();
        tile_system.instance.deal(PlayerBase.instance.CurrentPlayer, false);
            CheckInitiation(PlayerBase.instance.CurrentPlayer, false);
            currentGameState = GameState.playerTurn;
           
            ButtonManager.instance.CheckRiichiButton();
            if (PlayerBase.instance.CheckAI(PlayerBase.instance.CurrentPlayer))
                Ai_system.instance.Ai_Actons(PlayerBase.instance.CurrentPlayer);
        }

        public void CheckInitiation(int forPlayer, bool fromDiscard)
        {
            List<tile_system.comb> SingleTileCombs = CheckManager.instance.SingleTest(forPlayer, fromDiscard);

            List<tile_system.comb> MahjongTest = new List<tile_system.comb>();

                List<tile_system.comb> PossibleMahjong = new List<tile_system.comb>();
                PossibleMahjong.AddRange(PlayerBase.instance.GetPossibleCombs(forPlayer));

            if (SingleTileCombs.Count > 0)
            {
                PossibleMahjong.AddRange(SingleTileCombs);
                MahjongTest.AddRange(SingleTileCombs);
            }
            else if (PlayerBase.instance.GetDeclaredCombs(forPlayer).Count == 0)
            {
                tile_system.comb specialMJ = CheckManager.instance.SpecialMahjongChecker(forPlayer,fromDiscard);

                if (specialMJ != null)
                    MahjongTest.Add(specialMJ);
            }

                MahjongTest.AddRange(CheckManager.instance.MahjongChecker(forPlayer, fromDiscard, PossibleMahjong));

                foreach (tile_system.comb cmb in tile_system.instance.CombFilter(PlayerBase.instance.GetPossibleCombs(forPlayer), 4))
                {
                    MahjongTest.Add(cmb);
                }

            if (MahjongTest != null && MahjongTest.Count > 0)
            ButtonManager.instance.CombButtonCreator(forPlayer, false, MahjongTest);
        }

        public void DrawState()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }

        public void StateSwitcher (bool toDiscard)
        {
            if (toDiscard)
                currentGameState = GameState.DiscardCheck;
            else
                currentGameState = GameState.playerTurn;
        }

        public GameState State { get { return currentGameState; } }  
   

   public void CallRiichi()
    {
        ButtonManager.instance.CallRiichi();
    }

    
    
    public void HoverOverCombButton(bool turnOn, tile_system.comb comb)
    {
        if (comb != null)
        {

            for (int i = 0; i < comb.Tiles.Count(); i++)
            {
                
                ButtonManager.instance.ColorSelection(0, comb.Tiles[i], turnOn);
            }
        }

    }

 

    public IEnumerator Discard(tile_system.Tile tile, int playerDiscarded)
    {
        ButtonManager.instance.ClearCombButtons();
        //tile_system.TileSet.LastDrop = tile;
        ExecutionManager.instance.PrepareToRequest();
      

        for (int i = 0; i < PlayerBase.instance.player_count; i++)
        {
            if (i == playerDiscarded)
            {
                ExecutionManager.instance.SetRequest(i, null);
                if (!PlayerBase.instance.CheckAI(i))
                    ExecutionManager.instance.PlayerAct(null);

                continue;
            }
            else
            {
                List<tile_system.comb> TestFromDiscarded = CheckManager.instance.SingleTest(i, true);
                List<tile_system.comb> tempCombs = new List<tile_system.comb>();
                tempCombs.AddRange(TestFromDiscarded);
                tempCombs.AddRange(PlayerBase.instance.GetPossibleCombs(i));
                //tempCombs.AddRange(PlayerBase.playerManager.GetDeclaredCombs(i));
                if (tempCombs != null && tempCombs.Count > 0)
                {
                    if (!PlayerBase.instance.CheckAI(i))
                        {
                            var tempMahjong = CheckManager.instance.MahjongChecker(i, true, tempCombs);

                                 if (tempMahjong != null && tempMahjong.Count > 0) 
                                    {
                                     TestFromDiscarded.AddRange(tempMahjong);
                                    }

                            ButtonManager.instance.CombButtonCreator(i, true, TestFromDiscarded);
                         }
                    else
                        {

                        var tempMahjong = CheckManager.instance.MahjongChecker(i, true, tempCombs);

                        if (tempMahjong != null && tempMahjong.Count > 0)
                        {
                            TestFromDiscarded.AddRange(tempMahjong);
                        }

                        var tempFiltered = TestFromDiscarded.Where(temp => (int)temp.Type != 2 && (int)temp.Type != 5);

                        if (tile_system.instance.KanCounter >= 4)
                            tempFiltered = tempFiltered.Where(temp => (int)temp.Type != 4);

                        if (i != PlayerBase.instance.CurrentPlayer + 1 || (i == 0 && PlayerBase.instance.CurrentPlayer != 3))
                                tempFiltered = tempFiltered.Where(temp => (int)temp.Type != 1);

                        if (tempFiltered.Count() > 0)
                        {
                            int randomized = UnityEngine.Random.Range(0, tempFiltered.Count());
                            ExecutionManager.instance.SetRequest(i, tempFiltered.ElementAt(randomized));
                        }
                    }
                }
                else
                {
                    ExecutionManager.instance.SetRequest(i, null);

                    if (!PlayerBase.instance.CheckAI(i))
                        ExecutionManager.instance.PlayerAct(null);
                }
            }

        }

        counter.enabled = true;
        int Timer = 5;
        while (Timer > 0)
        {
            if (ExecutionManager.instance.HasPlayerActed())
            {
                Timer = 0;
                break;
            }
            print(Timer);
            ButtonManager.instance.CountOperator(Timer);
            Timer--;
            yield return new WaitForSeconds(1);
        }

        if (Timer <= 0)
        {
            counter.enabled = false;
            if (ExecutionManager.instance.HasPlayerActed())
            {
                ExecutionManager.instance.SetRequest(0, ExecutionManager.instance.GetChosen());
                ExecutionManager.instance.PrepareToAct();
            }
            else
            {
                ExecutionManager.instance.SetRequest(0, null);
            }

            ButtonManager.instance.ClearCombButtons();
            ButtonManager.instance.ColorClean();
            ExecutionManager.instance.PriorityExecution();
        }

    }

  

}
