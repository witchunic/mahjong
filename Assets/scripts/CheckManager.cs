﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using System.Linq;

public class CheckManager : MonoBehaviour {

    public static CheckManager instance;

    void Awake()
    {
        instance = this;
    }

    public bool CombIntersectCheck(tile_system.comb CombToCheck, List<tile_system.comb> ListToCompare)
    {
        foreach (var item in ListToCompare)
        {
            if (item.Tiles.Intersect(CombToCheck.Tiles).Count() > 0)
                return true;
        }
        return false;
    }

    public List<tile_system.comb> StaterTest(int Player)
    {
        IEnumerable<tile_system.Tile> TestedHand = PlayerBase.instance.GetHand(Player, 2, false);
        List<tile_system.comb> CombsFound = new List<tile_system.comb>();

        for (int i = 0; i < 13; i++)
        {
            if (TestedHand.ElementAt(i) == tile_system.instance.Zero)
                continue;

            var testedTiles = TestedHand.Except(new tile_system.Tile[] { TestedHand.ElementAt(i) });

            TestingPonKanPair(testedTiles, 2, TestedHand.ElementAt(i), CombsFound);
            TestingChow(testedTiles, 2, TestedHand.ElementAt(i), CombsFound);
        }

        return CombsFound;
    }

    public List<tile_system.comb> SingleTest(int Player, bool isDiscard)
    {
        List<tile_system.comb> CombosCounter = new List<tile_system.comb>();

        tile_system.Tile TestedTile = (isDiscard) ? PlayerBase.instance.GetTile(Player, 14) : PlayerBase.instance.GetTile(Player, 13);
        print("Testing " + TestedTile.Info + " for player " + (Player + 1));
        IEnumerable<tile_system.Tile> TestedHand = PlayerBase.instance.GetHand(Player, 2, false);


        TestingPonKanPair(TestedHand, 2, TestedTile, CombosCounter);
        TestingChow(TestedHand, 2, TestedTile, CombosCounter);

        PlayerBase.instance.StoreLastSingleTest(Player, CombosCounter);

        print("Found " + CombosCounter.Count);
        return CombosCounter;
    }

    void TestingPonKanPair(IEnumerable<tile_system.Tile> TestedHand, int MinimumComboType, tile_system.Tile TestedTile, List<tile_system.comb> CombosCounter)
    {
        if (TestedTile != tile_system.instance.Zero)
        {
            var TempCombo = TestedHand.Where(tile => TestedTile.Suit == tile.Suit && TestedTile.Number == tile.Number);
            if (TempCombo.Count() > 0)
            {
                //if (MinimumComboType < 3)
                //   CombosCounter.Add(new tile_system.comb(tile_system.CombType.pair, new tile_system.Tile[] { TestedTile, TempCombo.ElementAt(0) }));
                if (TempCombo.Count() > 1 && MinimumComboType < 4)
                    CombosCounter.Add(new tile_system.comb(tile_system.CombType.pon, new tile_system.Tile[] { TestedTile, TempCombo.ElementAt(0), TempCombo.ElementAt(1) }));
                if (TempCombo.Count() > 2)
                    CombosCounter.Add(new tile_system.comb(tile_system.CombType.kan, new tile_system.Tile[] { TestedTile, TempCombo.ElementAt(0), TempCombo.ElementAt(1), TempCombo.ElementAt(2) }));
            }
        }

    }

    List<tile_system.comb> GetAllPairs(IEnumerable<tile_system.Tile> TestedHand)
    {
        List<tile_system.comb> result = new List<tile_system.comb>();

        for (int outer = 0; outer < TestedHand.Count() - 1; outer++)
        {
            if (TestedHand.ElementAt(outer).Number == 0)
                continue;
            for (int inner = outer + 1; inner < TestedHand.Count(); inner++)
            {
                if (TestedHand.ElementAt(inner).Number == 0)
                    continue;

                if (TestedHand.ElementAt(outer).ID == TestedHand.ElementAt(inner).ID)
                    result.Add(new tile_system.comb(tile_system.CombType.pair, new tile_system.Tile[] { TestedHand.ElementAt(outer), TestedHand.ElementAt(inner) }));
            }
        }

        return result;

    }

    void TestingChow(IEnumerable<tile_system.Tile> TestedHand, int MinimumComboType, tile_system.Tile TestedTile, List<tile_system.comb> CombosCounter)
    {
        if (TestedTile != tile_system.instance.Zero)
        {
            var group = TestedHand.Where(tile => TestedTile.Suit == tile.Suit);

            if (group.Count() > 0)
            {
                if (TestedTile.Number < 10)
                {
                    for (int a1 = 0; a1 < group.Count(); a1++)
                    {
                        if (group.ElementAt(a1).Number > TestedTile.Number + 2 || group.ElementAt(a1).Number < TestedTile.Number - 2 || group.ElementAt(a1).Number == TestedTile.Number)
                            continue;

                        if (a1 < group.Count() - 1)
                        {
                            for (int a2 = a1 + 1; a2 < group.Count(); a2++)
                            {
                                if (group.ElementAt(a2).Number > TestedTile.Number + 2 || group.ElementAt(a2).Number < TestedTile.Number - 2 || group.ElementAt(a2).Number == TestedTile.Number)
                                    continue;


                                IEnumerable<tile_system.Tile> tempForChi = new tile_system.Tile[] { group.ElementAt(a1), group.ElementAt(a2), TestedTile }.OrderBy(tile => tile.Number);
                                if (tempForChi.ElementAt(0).Number + 1 == tempForChi.ElementAt(1).Number && tempForChi.ElementAt(1).Number + 1 == tempForChi.ElementAt(2).Number)
                                    CombosCounter.Add(new tile_system.comb(tile_system.CombType.chi, new tile_system.Tile[] { tempForChi.ElementAt(0), tempForChi.ElementAt(1), tempForChi.ElementAt(2) }));

                            }
                        }
                    }
                }


            }
        }
    }

    public List<List<tile_system.comb>> CombIterator(int player, List<tile_system.comb> ListOfCombs, bool forMahjong)
    {

        List<List<tile_system.comb>> resultCombs = new List<List<tile_system.comb>>();
        List<tile_system.comb> iterator;

        for (int loopOuter = 0; loopOuter < ListOfCombs.Count; loopOuter++)
        {
            iterator = new List<tile_system.comb>();
            iterator.AddRange(PlayerBase.instance.GetDeclaredCombs(player));
            iterator.Add(ListOfCombs[loopOuter]);
            for (int loopInner = loopOuter + 1; loopInner < ListOfCombs.Count; loopInner++)
            {
                if (CombIntersectCheck(ListOfCombs[loopInner], iterator))
                    continue;
                else
                    iterator.Add(ListOfCombs[loopInner]);

                if (iterator.Count == 4)
                {
                    resultCombs.Add(iterator);
                    break;
                }
            }

            print("iterator : " + iterator.Count);
            if (iterator.Count > 2)
                resultCombs.Add(iterator);
        }


        return resultCombs;
    }

    public List<tile_system.comb> MahjongChecker(int player, bool fromDiscard, List<tile_system.comb> listOfCombs)
    {
        List<tile_system.comb> result = new List<tile_system.comb>();
        List<List<tile_system.comb>> sortedCombs = CombIterator(player, listOfCombs, true);
        print("Found " + sortedCombs.Count + " mahjong variations to check");

        for (int i = 0; i < sortedCombs.Count; i++)
        {

            int TempCountChiPon = sortedCombs[i].Count;
            int pairCount = 0;
            int pairLimit = (sortedCombs.Count == 3) ? 2 : 1;

            List<tile_system.comb> tempPairs = GetAllPairs(PlayerBase.instance.GetHand(player, 1, fromDiscard));

            if (tempPairs != null && tempPairs.Count > 0)
            {
                foreach (var item in tempPairs)
                {
                    if (CombIntersectCheck(item, sortedCombs[i]))
                        continue;
                    else
                    {
                        sortedCombs[i].Add(item);
                        pairCount++;
                        if (pairCount >= pairLimit)
                            break;
                    }

                }
            }

            MJprinter(sortedCombs[i]);

            if (sortedCombs[i].Count == 5)
            {
                if (pairCount > 1 && player == PlayerBase.instance.CurrentPlayer)
                {//TenpaiCase(sortedCombs[i],player,fromDiscard);

                    TilePrinter(TenpaiCase(sortedCombs[i], player, fromDiscard));

                }
                else if (pairCount == 1)
                {
                    print("Found Mahjong! For " + (player + 1) + " player!");
                    MJprinter(sortedCombs[i]);
                    tile_system.Tile tempTile = (fromDiscard) ? PlayerBase.instance.GetTile(player, 14) : PlayerBase.instance.GetTile(player, 13);
                    result.Add(new tile_system.comb(tile_system.CombType.mahjong, new tile_system.Tile[] { tempTile }));
                }
            }
            else if (sortedCombs[i].Count == 4 && player == PlayerBase.instance.CurrentPlayer)
            {
                //TenpaiCase(sortedCombs[i],player,fromDiscard);

                TilePrinter(TenpaiCase(sortedCombs[i], player, fromDiscard));
            }
        }

        return result;
    }

    public tile_system.comb SpecialMahjongChecker(int player, bool fromDiscard)
    {
        tile_system.comb result = null;

        if (PlayerBase.instance.GetDeclaredCombs(player).Count == 0)
        {
            List<tile_system.comb> pairs = GetAllPairs(PlayerBase.instance.GetHand(player, 1, fromDiscard));
            if (pairs.Count > 5)
            {
                List<tile_system.comb> sortedPairs;

                foreach (var item in pairs)
                {
                    sortedPairs = new List<tile_system.comb>();
                    sortedPairs.Add(item);
                    for (int i = 1; i < pairs.Count; i++)
                    {
                        if (CombIntersectCheck(pairs[i], sortedPairs))
                            continue;
                        else
                        {
                            bool duplicate = false;

                            foreach (var comb in sortedPairs)
                            {
                                if (comb.Info == pairs[i].Info)
                                {
                                    duplicate = true;
                                    break;
                                }
                            }

                            if (!duplicate)
                                sortedPairs.Add(pairs[i]);
                        }
                    }

                    if (sortedPairs.Count == 7)
                    {
                        print("Found mahjong (7 pairs) for player " + player + "!");
                        result = new tile_system.comb(tile_system.CombType.mahjong, new tile_system.Tile[] { sortedPairs[0].Tiles[0] });
                        MJprinter(sortedPairs);
                        break;
                    }
                    else if (sortedPairs.Count == 6 && player == PlayerBase.instance.CurrentPlayer)
                    {

                        print("Tenpai on 6 pairs!");
                        //MJprinter(sortedPairs);
                        TilePrinter(TenpaiCase(sortedPairs, player, fromDiscard));

                    }


                }
            }

        }

        return result;
    }



    List<tile_system.Tile> TenpaiCase(List<tile_system.comb> testedCombs, int player, bool fromDiscard)
    {
        int combs = 0;
        int pairs = 0;
        bool tenpai = false;
        List<tile_system.Tile> waitings = new List<tile_system.Tile>();

        foreach (var item in testedCombs)
        {
            if (item.Type == tile_system.CombType.pair)
                pairs++;
            else
                combs++;
        }

        if (combs == 4 && pairs == 0)
        {
            print("Tenpai! Need pairs!");
            tenpai = true;
            //остаток - две возможные пары
            List<tile_system.comb> tempcombs = testedCombs;
            foreach (var item in PlayerBase.instance.GetDeclaredCombs(player))
            {
                tempcombs.Remove(item);
            }
            List<tile_system.Tile> remainder = TileSeparator(PlayerBase.instance.GetHand(player, 1, fromDiscard), tempcombs);
            if (remainder != null && remainder.Count > 0)
            {
                waitings = remainder;
            }

        }
        else if (combs == 3 && pairs == 2)
        {
            print("Tenpai! Convert pair to chi!");
            tenpai = true;
            //две пары - ожидание пона
            List<tile_system.comb> filter = tile_system.instance.CombFilter(testedCombs, 2);
            foreach (var item in filter)
            {
                waitings.Add(item.Tiles[0]);
            }

        }
        else if (pairs == 6)
        {
            print("Tenpai on 6 pairs!");
            tenpai = true;
            //остаток две возможные пары
            List<tile_system.Tile> remainder = TileSeparator(PlayerBase.instance.GetHand(player, 1, fromDiscard), testedCombs);
            if (remainder != null && remainder.Count > 0)
            {
                bool r1dup = false;
                bool r2dup = false;
                foreach (var item in testedCombs)
                {
                    if (!r1dup && remainder[0].Info == item.Tiles[0].Info)
                        r1dup = true;

                    if (!r2dup && remainder[1].Info == item.Tiles[0].Info)
                        r2dup = true;

                    if (r1dup && r2dup)
                        break;
                }

                if (!r1dup)
                    waitings.Add(remainder[0]);
                if (!r2dup)
                    waitings.Add(remainder[1]);

            }
        }
        else if (combs == 3 && pairs == 1)
        {
            print("Can be tenpai! Need To check halfChi!");
            MJprinter(testedCombs);
            List<tile_system.Tile> tempTiles = TileSeparator(PlayerBase.instance.GetHand(player, 1, fromDiscard), testedCombs);
            if (tempTiles != null && tempTiles.Count > 0)
            {
                if (tempTiles[0].Suit == tempTiles[1].Suit)
                {
                    if (tempTiles[0].ID != tempTiles[1].ID && (tempTiles[0].Number < tempTiles[1].Number + 2 && tempTiles[0].Number > tempTiles[1].Number - 2))
                    {
                        print("tenpai on halfchi!");

                        tenpai = true;

                        if (tempTiles[0].Number == tempTiles[1].Number + 1)
                        {
                            if (tempTiles[0].Number > 1)
                                waitings.Add(tile_system.instance.GetTileById(tempTiles[0].ID - 1));

                            if (tempTiles[1].Number < 9)
                                waitings.Add(tile_system.instance.GetTileById(tempTiles[1].ID + 1));
                        }
                        else
                        {
                            waitings.Add(tile_system.instance.GetTileById(tempTiles[0].ID + 1));

                        }
                    }
                }
            }
            //остаток - возможное чи
        }

        if (tenpai)
            MJprinter(testedCombs);

        // List<tile_system.Tile> remainder = TileSeparator(PlayerBase.playerManager.GetHand(player, 1, fromDiscard), testedCombs);

        /*  if (remainder != null && remainder.Count > 0)
          {

          }
          */
        if (player == 0)
            ButtonManager.instance.WaitingManager(player, waitings);

        PlayerBase.instance.SetTenpai(player, tenpai);
        return waitings;
    }

    List<tile_system.Tile> TileSeparator(IEnumerable<tile_system.Tile> original, List<tile_system.comb> cleaner)
    {
        

        List<tile_system.Tile> result = new List<tile_system.Tile>();
        foreach (var item in cleaner)
        {
            result.AddRange(item.Tiles);
        }

        return result.Intersect(original).ToList();
    }



    public void MJprinter(List<tile_system.comb> combsToPrint)
    {
        string result = "Tenpai on : ";
        foreach (var item in combsToPrint)
        {
            result += item.Info + " ";
        }

        print(result);
    }

    

    public void TilePrinter(List<tile_system.Tile> TileToPrint)
    {
        string result = "Waiting for : ";
        foreach (var item in TileToPrint)
        {
            result += item.Info + " ";
        }

        print(result);
    }
}
