﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PlayerBase : MonoBehaviour {


    public static PlayerBase instance;

    public int player_count = 2;
    public int ai_players = 1;
    player[] playerList;
    int currentPlayer = -1;

    void Awake ()
    {
        instance = this;
    }

    
    public class player
    {
        int id;
        public int score;
        tile_system.suit seat;
        bool ai;

        List<tile_system.Tile> hand_main = new List<tile_system.Tile>(15) { tile_system.instance.Zero, tile_system.instance.Zero, tile_system.instance.Zero, tile_system.instance.Zero, tile_system.instance.Zero, tile_system.instance.Zero, tile_system.instance.Zero, tile_system.instance.Zero, tile_system.instance.Zero, tile_system.instance.Zero, tile_system.instance.Zero, tile_system.instance.Zero, tile_system.instance.Zero, tile_system.instance.Zero, tile_system.instance.Zero };
        List<tile_system.Tile> hand_discarded = new List<tile_system.Tile>();
        List<tile_system.comb> declaredCombs = new List<tile_system.comb>();
        List<tile_system.comb> possibleCombs = new List<tile_system.comb>();
        List<tile_system.comb> lastSingleTest;
        List<tile_system.Tile> waitings = new List<tile_system.Tile>();
        bool riichi = false;
        bool tenpai = false;

        public player(int id, int score, bool ai)
        {
            this.id = id;
            this.score = score;
            this.ai = ai;
        }

        public int ID { get { return id; }}

        public int Score {get { return score; } set { score += value; }}

        public bool Tenpai { get { return tenpai; } set { tenpai = value; } }

        public bool Riichi { get { return riichi; } set { riichi = value; } }

        public List<tile_system.Tile> Waitings { get { return waitings; } set { waitings = value; } }

        public bool AI { get { return ai; } }

        public void dealTile(int position, tile_system.Tile tile)
        {
            hand_main[position] = tile;
            hand_main[14] = tile_system.instance.Zero;
            
        }

        public void Sorter()
        {
            List<tile_system.Tile> sortedPart = hand_main.Take(13).OrderBy(tile => tile.Suit).ThenBy(tile => tile.Number).ToList();
            for (int i = 0; i < 13; i++)
            {
                hand_main[i] = sortedPart[i];
            }
        }

        public void Discard(int tilePlace)
        {
            print("Player " + (instance.CurrentPlayer+1) + " discarded " + hand_main[tilePlace-1].Info);
            tile_system.Tile discarded = hand_main[tilePlace - 1];
            hand_discarded.Add(discarded);


            if (tilePlace < 14)
            {
                hand_main[tilePlace - 1] = hand_main[13];
                if (lastSingleTest != null)
                possibleCombs.AddRange(lastSingleTest);
            }

            hand_main[13] = tile_system.instance.Zero;

            Sorter();
            //  possibleCombs.RemoveAll(comb => comb.Tiles.Contains(discarded));
            possibleCombs = CheckManager.instance.StaterTest(id);
            ButtonManager.instance.ButtonMaker();
            ExecutionManager.instance.DiscardHappened(discarded);
        }

        public IEnumerable<tile_system.Tile> GetHand(int fullOrPartOrBase, bool isDiscard)
        {
            if (fullOrPartOrBase == 2)
                return hand_main.Take(13);
            else if (fullOrPartOrBase == 1)
            {
                List<tile_system.Tile> tempHand = hand_main.Take(13).ToList();

                if (isDiscard)
                    tempHand.Add(hand_main[14]);
                else
                    tempHand.Add(hand_main[13]);


                return tempHand;
            }
            else
                return hand_main;
        }

        public tile_system.Tile GetTile(int position)
        {
            return hand_main[position];
        }

        public tile_system.Tile LastDiscard {get { return hand_main[14]; } set { hand_main[14] = value; } }

        public List<tile_system.comb> PossibleCombs {get { return possibleCombs; } set { possibleCombs = value; } }

        public List<tile_system.comb> DeclaredCombs { get { return declaredCombs; } }
  
        public void DeclareComb(tile_system.CombType type, tile_system.Tile[] tiles)
        {
            declaredCombs.Add(new tile_system.comb(type, tiles));

            foreach (var item in tiles)
                {
                //hand_main[System.Array.IndexOf(hand_main, item)] = tile_system.TileSet.Zero;
                hand_main[hand_main.IndexOf(item)] = tile_system.instance.Zero;
                //possibleCombs.RemoveAll(comb => comb.Tiles.Contains(item));
                possibleCombs = CheckManager.instance.StaterTest(id);
               }

            Sorter();
        }

        public List<tile_system.comb> LastSingleTest {
            get { return lastSingleTest; } set { lastSingleTest = value; } } 

    }

    // tile_system.suit[] WindsAndSeats = new tile_system.suit[] {tile_system.suit.east, tile_system.suit.north, tile_system.suit.west, tile_system.suit.south};
    

    public void CreatePlayers(int number, int aiPlayers)
    {
        playerList = new player[number];

        for (int i = 0; i < number; i++)
        {
            bool isAI = false;

            if (i >= number - aiPlayers)
                isAI = true;

            playerList[i] = new player(i, 25000, isAI);


        }
    }

    public IEnumerable<tile_system.Tile> GetHand(int player, int fullOrPartOrBase, bool isDiscard)
    {
        return playerList[player].GetHand(fullOrPartOrBase, isDiscard);
    }

    public void DealTile(int player, int position, tile_system.Tile tile)
    {
        playerList[player].dealTile(position, tile);
    }

    public tile_system.Tile GetTile(int player, int position)
    {
        return playerList[player].GetTile(position);
    }

    public void Discard(int player, int position)
    {
        game_system.instance.StateSwitcher(true);
        playerList[player].Discard(position);
    }

    public void StarterDeal()
    {
        for (int player = 0; player < PlayerBase.instance.player_count; player++)
        {
            for (int i = 0; i < 13; i++)
            {
                DealTile(player, i, tile_system.instance.currentDeal());
            }

            playerList[player].Sorter();
            SetPossibleCombs(player, CheckManager.instance.StaterTest(player), true);
        }
    }

    public List<tile_system.comb> GetPossibleCombs(int player)
    {
        return playerList[player].PossibleCombs;
    }

    public void SetPossibleCombs(int player, List<tile_system.comb> combs, bool clear)
    {
        if (clear)
            playerList[player].PossibleCombs.Clear();

        playerList[player].PossibleCombs.AddRange(combs);
    }

    public void SetLastDiscard(tile_system.Tile tile)
    {
        foreach (var item in playerList)
        {
            item.LastDiscard = tile;

        }
    }

    public List<tile_system.comb> GetDeclaredCombs(int player)
    {
        return playerList[player].DeclaredCombs;
    }

    public void DeclareCombs(int player, tile_system.CombType type, tile_system.Tile[] tiles)
    {
        playerList[player].DeclareComb(type, tiles);
    }

    public void StoreLastSingleTest(int player, List<tile_system.comb> singleTestList)
    {
        playerList[player].LastSingleTest = singleTestList;
    }

    public int CurrentPlayer
    {
        get
        {
            return currentPlayer;
        }
    }

    public void TurnSwitch(bool Steal, int player)
    {
        if (Steal)
            currentPlayer = player;
        else
            currentPlayer = (currentPlayer == player_count-1) ? currentPlayer = 0 : currentPlayer + 1;

    }

    public void SetTenpai(int player, bool tenpai)
    {
        playerList[player].Tenpai = tenpai;
    }
    public bool GetTenpai(int player)
    {
        return playerList[player].Tenpai;
    }

    public bool IsRiichi(int player)
    {
        return playerList[player].Riichi;
    }

    public void SetRiichi(int player, bool riichi)
    {
        playerList[player].Riichi = riichi;
    }

    public List<tile_system.Tile> GetWaitings(int player)
    {
        return playerList[player].Waitings;
    }

    public void SetWaitings(int player, List<tile_system.Tile> waitings)
    {
        playerList[player].Waitings = waitings;
    }

    public bool CheckAI(int player)
    {
        if (playerList[player].AI)
            print("Player " + (player + 1) + " is AI!");

        return playerList[player].AI;
    }
}
